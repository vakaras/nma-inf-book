Kompiliavimas
=============

::
  make

Stiliaus reikalavimai
=====================

#. Koduotė UTF-8.
#. Eilučių pabaigos simboliai: Unix.
#. Eilutės ne ilgesnės nei 80 simbolių

  * Išimtis gali būti taikoma jei reikia įterpti lentelę.

#. Skyrybos ženklai: ``„“–…``
#. Matematikos formulės: ``:math:`LaTeX formulė```
#. Uždavinių formuluotės:

  * Paprastas šriftas (nenaudoti kursyvo).
  * Atskirti nuo krašto (quoted paragraph stilius).

#. Kodo pavyzdžiai:

  * ``.. code-block:: unicode_pascal``,
  * 4 tarpų indentacija (be tab'ų),
